#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """

        """
        Hago un diccionario formado por etiquetas
        y sus atributos metidos en listas
        """
        self.diccionario = {"root-layout": ["width", "height",
                            "blackground_color"],
                            "region": ["id", "top", "bottom", "left", "right"],
                            "img": ["src", "region", "begin", "dur"],
                            "audio": ["src", "begin", "dur"],
                            "textstream": ["src", "region"]}
        self.lista = []

    def startElement(self, etiqueta, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """

        """
        Creo un nuevo diccionario donde añado las etiquetas y sus atributos
        """
        if etiqueta in self.diccionario:
            diccionario = {}
            for atributo in self.diccionario[etiqueta]:
                diccionario[atributo] = attrs.get(atributo, '')
            self.lista.append([etiqueta, diccionario])

    def get_tags(self):
        return self.lista


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(sys.argv[1]))
    DATOS = cHandler.get_tags()
    print(DATOS)
