#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
import sys
import json
import urllib.request
import smallsmilhandler


class KaraokeLocal:
    """ Inicialización de variables. """
    def __init__(self, file):
        parser = make_parser()
        cHandler = smallsmilhandler.SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(file))
        self.datos = cHandler.get_tags()

    """ Devuelve un string a partir de la lista de diccionarios """
    def __str__(self):
        new_line = ''
        for diccionario in self.datos:   # Muestra cada linea de datos
            etiqueta = diccionario[0]    # [0]=etiqueta, [1]=atributos/valores
            new_line += etiqueta        # Items devuelve atributo/valor
            for atributo, valor in diccionario[1].items():
                new_line = new_line + '\t' + atributo + "=" + '"' + valor + '"'
            new_line += '\n'
        return new_line

    """ Guarda los datos en un fichero JSON """
    def do_json(self, file):
        file_json = file.replace('.smil', '.json')  # Cambia extension de file
        with open(file_json, 'w') as fich_json:  # Codifico lista en nuevo file
            json.dump(self.datos, fich_json, indent=4)  # Indent = tabulacion

    """ Descarga los recursos remotos """
    def do_local(self):
        for diccionario in self.datos:
            for atributo, valor in diccionario[1].items():
                if atributo == 'src' and valor[0:7] == 'http://':
                    new_url = valor.split('/')[-1]  # Se queda con [hello.jpg]
                    urllib.request.urlretrieve(valor, new_url)
                    print("Descargando...")
                    diccionario[1]['src'] = new_url
                    # Urllib descarga el archivo
                    # Urlretrieve lo copia a un archivo local


if __name__ == "__main__":
    """
    Programa principal
    """
    try:
        file = sys.argv[1]
    except IndexError:
        sys.exit("Usage: python3 karaoke.py file.smil.")

    KARAOKE = KaraokeLocal(file)
    print(KARAOKE)
    KARAOKE.do_json(file)
    KARAOKE.do_local()
    KARAOKE.do_json('local.json')
    print(KARAOKE)
